﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppApi.App_Start
{
    public static class Enums
    {
        public enum ProfileStatus
        {
            Active,
            InActive,
            Deleted,
            New,
            LiveNow,
            Completed,
            Pending,
            Accepted,
            Rejected
        }

        public enum UserRole
        {
            User,
            Celebrity,
            Admin
        }

        public enum WalletType
        {
            PromoCode,
            ReferrerUserId,
            SignedUpWithReferUserId
        }

        public enum PolicyParameters
        {
            ReferrerBonus
        }

        public enum ResponseCode
        {
            Success,
            Failure,
            Unauthorized,
            Exception,
            Info,
            NotExists
        }

        public enum Notificationtype
        {
            FriendRequest,
            ContestRequest,
            AcceptedRequest
        }
    }
}