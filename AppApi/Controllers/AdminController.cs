﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AppApi.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult UserLogin()
        {
            return View();
        }
        public ActionResult UserRegister()
        {
            return View();
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Users()
        {
            return View();
        }
        public ActionResult AdminUsers()
        {
            return View();
        }
        public ActionResult PetList()
        {
            return View();
        }
        public ActionResult ServiceList()
        {
            return View();
        }
        public ActionResult login()
        {
            return View();
        }
    }
}