﻿using AppApi.App_Start;
using AppApi.Models;
using AppApi.Models.input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AppApi.Controllers
{
    public class AjaxController : Controller
    {
        // GET: Ajax
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult adminlogin(string UserName,string Password)
        {
            try
            {
                AdminUser adminEntity = new AdminUser();
                using (var con = new PetServicesEntities())
                {
                    adminEntity = con.AdminUsers.Where(x => x.UserName == UserName && x.Password == Password).FirstOrDefault();
                    if (adminEntity != null)
                    {

                        return Json(new AjaxResponse(true, "Successfully Login."));
                    }
                    else
                    {
                        return Json(new AjaxResponse(false, "Invalid UserName or Password."));
                    }

                    
                }



            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }

        [HttpGet]
        public JsonResult GetAdminList()
        {
            try
            {
                PetServicesEntities dbcontext = new PetServicesEntities();
                List<AdminUser> entity = new List<AdminUser>();
                entity = dbcontext.AdminUsers.ToList();
                if (entity != null)
                {
                    return Json(new AjaxResponse(true, "", true, entity), JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(new AjaxResponse(false, "UserName or Password Incorrect"));
                }

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }
        [HttpPost]
        public JsonResult Login(inputAdmin param)
        {
            try
            {
                PetServicesEntities dbcontext = new PetServicesEntities();
                AdminUser entity = dbcontext.AdminUsers.Where(x => x.UserName == param.UserName && x.Password == param.Password).FirstOrDefault();
                if (entity != null)
                {
                    Redirect("/Admin/AdminLogin/");
                    return Json(new AjaxResponse(true, "true"));
                }
                else
                {
                    return Json(new AjaxResponse(false, "UserName or Password Incorrect"));
                }

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }
        [HttpPost]
        public JsonResult AddAdmin(inputAdmin param)
        {
            try
            {
                PetServicesEntities dbcontext = new PetServicesEntities();
                AdminUser entity = dbcontext.AdminUsers.Where(x => x.UserName == param.UserName).FirstOrDefault();
                if (entity != null)
                {
                    return Json(new AjaxResponse(false, "User Already Exist."));
                }
                else
                {
                    AdminUser adEntity = new AdminUser();
                    adEntity.FirstName = param.FirstName;
                    adEntity.UserName = param.UserName;
                    adEntity.LastName = param.LastName;
                    adEntity.Password = param.Password;
                    adEntity.UserContact = param.UserContact;
                    adEntity.UserGender = param.UserGender;

                    dbcontext.AdminUsers.Add(adEntity);
                    dbcontext.SaveChanges();

                    return Json(new AjaxResponse(true, "Seccessfully Created."));
                }

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }
        [HttpPost]
        public JsonResult UpdateAdmin(inputAdmin param)
        {
            try
            {
                PetServicesEntities dbcontext = new PetServicesEntities();
                AdminUser entity = dbcontext.AdminUsers.Where(x => x.AdminID == param.AdminID).FirstOrDefault();
                if (entity != null)
                {
                    entity.FirstName = param.FirstName;
                    entity.LastName = param.LastName;
                    entity.Password = param.Password;
                    entity.UserContact = param.UserContact;
                    entity.UserGender = param.UserGender;
                    dbcontext.SaveChanges();


                    return Json(new AjaxResponse(true, "true"));
                }
                else
                {
                    return Json(new AjaxResponse(false, "UserName or Password Incorrect"));
                }

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }
        [HttpPost]
        public JsonResult DeleteAdmin(int AdminID)
        {
            try
            {
                PetServicesEntities dbcontext = new PetServicesEntities();
                AdminUser entity = dbcontext.AdminUsers.Where(x => x.AdminID == AdminID).FirstOrDefault();

                if (entity != null)
                {
                    dbcontext.AdminUsers.Remove(entity);
                    dbcontext.SaveChanges();
                    return Json(new AjaxResponse(true, "true"));
                }
                else
                {
                    return Json(new AjaxResponse(false, "UserName or Password Incorrect"));
                }

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }

        [HttpPost]
        public JsonResult EditAdmin(int AdminID)
        {
            try
            {
                PetServicesEntities dbcontext = new PetServicesEntities();
                AdminUser entity = dbcontext.AdminUsers.Where(x => x.AdminID == AdminID).FirstOrDefault();

                if (entity != null)
                {
                    return Json(new AjaxResponse(true, "", true, entity), JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(new AjaxResponse(false, "Something Wrong."));
                }

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }
        [HttpGet]
        public JsonResult GetUserList()
        {
            try
            {
                PetServicesEntities dbcontext = new PetServicesEntities();
                List<User> entity = new List<User>();
                entity = dbcontext.Users.ToList();
                if (entity != null)
                {
                    return Json(new AjaxResponse(true, "", true, entity), JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(new AjaxResponse(false, "No Record Found."));
                }

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }

        [HttpGet]
        public JsonResult GetPetList()
        {
            try
            {
                List<SpGetPetList_Result> petlist = new List<SpGetPetList_Result>();
                using (var con = new PetServicesEntities())
                {
                    petlist = con.SpGetPetList().ToList();

                    return Json(new AjaxResponse(true, "", true, petlist), JsonRequestBehavior.AllowGet);
                }
                
              return Json(new AjaxResponse(false, "No Record Found."));

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }

        [HttpGet]
        public JsonResult GetPetServicesList()
        {
            try
            {
                List<SpGetPetServicesList_Result> petserviceslist = new List<SpGetPetServicesList_Result>();
                using (var con = new PetServicesEntities())
                {
                    petserviceslist = con.SpGetPetServicesList().ToList();

                    return Json(new AjaxResponse(true, "", true, petserviceslist), JsonRequestBehavior.AllowGet);
                }

                return Json(new AjaxResponse(false, "No Record Found."));

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }

        [HttpGet]
        public JsonResult GetAllImagesByPetId(int PetProfileId)
        {
            try
            {
                List<ProfileImage> ProfileImages = new List<ProfileImage>();
                using (var con = new PetServicesEntities())
                {
                    ProfileImages = con.ProfileImages.Where(x => x.PetProfileTypeId == 1 && x.PetProfileId == PetProfileId).ToList();

                    return Json(new AjaxResponse(true, "", true, ProfileImages), JsonRequestBehavior.AllowGet);
                }

                return Json(new AjaxResponse(false, "No Record Found."));

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }

        [HttpGet]
        public JsonResult RemoveProfileByProfileId(int PetProfileId)
        {
            try
            {
                PetProfile petprofile = new PetProfile();
                using (var con = new PetServicesEntities())
                {
                    petprofile = con.PetProfiles.Where(x => x.PetProfileId == PetProfileId).FirstOrDefault();
                    petprofile.StatusProfile = Enums.ProfileStatus.Deleted.ToString();
                    con.SaveChanges();
                    return Json(new AjaxResponse(false, "Approved Successfully."));
                }

                return Json(new AjaxResponse(false, "No Record Found."));

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }

        [HttpGet]
        public JsonResult ApprovedProfileByProfileId(int PetProfileId)
        {
            try
            {
                PetProfile petprofile = new PetProfile();
                using (var con = new PetServicesEntities())
                {
                    petprofile = con.PetProfiles.Where(x => x.PetProfileId == PetProfileId).FirstOrDefault();
                    petprofile.StatusProfile = Enums.ProfileStatus.Accepted.ToString();
                    con.SaveChanges();
                    return Json(new AjaxResponse(false, "Approved Successfully."));
                }

                return Json(new AjaxResponse(false, "No Record Found."));

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }

        [HttpGet]
        public JsonResult RemoveImageByProfileId(int PetProfileImageId)
        {
            try
            {
                ProfileImage ProfileImages = new ProfileImage();
                using (var con = new PetServicesEntities())
                {
                    ProfileImages = con.ProfileImages.Where(x => x.PetImageId == PetProfileImageId).FirstOrDefault();
                    con.ProfileImages.Remove(ProfileImages);
                    con.SaveChanges();
                    return Json(new AjaxResponse(true, "Removed Seccessfully."));
                }

                return Json(new AjaxResponse(false, "No Record Found."));

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }

        [HttpGet]
        public JsonResult GetAllServiceImagesByPetId(int PetServiceProfileId)
        {
            try
            {
                List<ProfileImage> ProfileImages = new List<ProfileImage>();
                using (var con = new PetServicesEntities())
                {
                    ProfileImages = con.ProfileImages.Where(x => x.PetProfileTypeId == 2 && x.PetProfileId == PetServiceProfileId).ToList();

                    return Json(new AjaxResponse(true, "", true, ProfileImages), JsonRequestBehavior.AllowGet);
                }

                return Json(new AjaxResponse(false, "No Record Found."));

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }

        [HttpGet]
        public JsonResult RemoveServiceProfileByProfileId(int PetServiceProfileId)
        {
            try
            {
                PetCareServicesProfile petprofile = new PetCareServicesProfile();
                using (var con = new PetServicesEntities())
                {
                    petprofile = con.PetCareServicesProfiles.Where(x => x.PetCareServicesId == PetServiceProfileId).FirstOrDefault();
                    petprofile.StatusProfile = Enums.ProfileStatus.Deleted.ToString();
                    con.SaveChanges();
                    return Json(new AjaxResponse(false, "Approved Successfully."));
                }

                return Json(new AjaxResponse(false, "No Record Found."));

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }

        [HttpGet]
        public JsonResult ApprovedServiceProfileByProfileId(int PetServiceProfileId)
        {
            try
            {
                 PetCareServicesProfile petprofile= new PetCareServicesProfile();
                using (var con = new PetServicesEntities())
                {
                    petprofile = con.PetCareServicesProfiles.Where(x => x.PetCareServicesId == PetServiceProfileId).FirstOrDefault();
                    petprofile.StatusProfile = Enums.ProfileStatus.Accepted.ToString();
                    con.SaveChanges();
                    return Json(new AjaxResponse(false, "Approved Successfully."));
                }

                return Json(new AjaxResponse(false, "No Record Found."));

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }

        [HttpGet]
        public JsonResult RemoveServiceImageByProfileId(int PetServiceProfileImageId)
        {
            try
            {
                ProfileImage ProfileImages = new ProfileImage();
                using (var con = new PetServicesEntities())
                {
                    ProfileImages = con.ProfileImages.Where(x => x.PetImageId == PetServiceProfileImageId).FirstOrDefault();
                    con.ProfileImages.Remove(ProfileImages);
                    con.SaveChanges();
                    return Json(new AjaxResponse(true, "Removed Seccessfully."));
                }

                return Json(new AjaxResponse(false, "No Record Found."));

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }

        [HttpPost]
        public JsonResult UpdateUser(inputAdmin param)
        {
            try
            {
                PetServicesEntities dbcontext = new PetServicesEntities();
                AdminUser entity = dbcontext.AdminUsers.Where(x => x.AdminID == param.AdminID).FirstOrDefault();
                if (entity != null)
                {
                    entity.FirstName = param.FirstName;
                    entity.LastName = param.LastName;
                    entity.Password = param.Password;
                    entity.UserContact = param.UserContact;
                    entity.UserGender = param.UserGender;
                    dbcontext.SaveChanges();


                    return Json(new AjaxResponse(true, "true"));
                }
                else
                {
                    return Json(new AjaxResponse(false, "UserName or Password Incorrect"));
                }

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }
        [HttpPost]
        public JsonResult DeleteUser(int AdminID)
        {
            try
            {
                PetServicesEntities dbcontext = new PetServicesEntities();
                AdminUser entity = dbcontext.AdminUsers.Where(x => x.AdminID == AdminID).FirstOrDefault();

                if (entity != null)
                {
                    dbcontext.AdminUsers.Remove(entity);
                    dbcontext.SaveChanges();
                    return Json(new AjaxResponse(true, "true"));
                }
                else
                {
                    return Json(new AjaxResponse(false, "UserName or Password Incorrect"));
                }

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }

        [HttpPost]
        public JsonResult EditUser(int AdminID)
        {
            try
            {
                PetServicesEntities dbcontext = new PetServicesEntities();
                AdminUser entity = dbcontext.AdminUsers.Where(x => x.AdminID == AdminID).FirstOrDefault();

                if (entity != null)
                {
                    return Json(new AjaxResponse(true, "", true, entity), JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(new AjaxResponse(false, "Something Wrong."));
                }

            }
            catch (Exception ex)
            {

                return Json(new AjaxResponse(false, "false"));
            }
        }
    }
}