﻿using Microsoft.Owin.Security.DataHandler.Encoder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace AppApi.Models
{
    public static class UserLogics
    {

        static UserLogics()
        {
            //AudiencesList.TryAdd("099153c2625149bc8ecb3e85e03f0022",
            //                    new Audience { ClientId = "099153c2625149bc8ecb3e85e03f0022", 
            //                                    Base64Secret = "IxrAjDoa2FqElO7IhrSrUJELhUckePEPVpaePlS_Xaw", 
            //                                    Name = "ResourceServer.Api 1" });
        }

        public static bool AddAudience(User usermodel)
        {
            try
            {
                var clientId = Guid.NewGuid().ToString("N");

                var key = new byte[32];
                RNGCryptoServiceProvider.Create().GetBytes(key);
                var base64Secret = TextEncodings.Base64Url.Encode(key);
                PetServicesEntities dbContext = new PetServicesEntities();

                User Userlist = dbContext.Users.FirstOrDefault(x => x.UserName == usermodel.UserName);
                if (Userlist != null)
                {
                    return false;
                }
                else
                {
                    User newAudience = new User
                    {
                        UserID = clientId,
                        Secret = base64Secret,
                        UserName = usermodel.UserName,
                        Password = usermodel.Password,
                        SurName = usermodel.SurName,
                        FullName = usermodel.FullName,
                        Country = usermodel.Country,
                        City = usermodel.City
                    };
                    dbContext.Users.Add(newAudience);
                    dbContext.SaveChanges();

                    return true;

                }
            }
            catch (Exception ex)
            {

                return false;
            }



        }

        public static string CheckUser(string UserName)
        {
            try
            {
                var clientId = Guid.NewGuid().ToString("N");

                var key = new byte[32];
                RNGCryptoServiceProvider.Create().GetBytes(key);
                var base64Secret = TextEncodings.Base64Url.Encode(key);
                PetServicesEntities dbContext = new PetServicesEntities();

                User Userlist = dbContext.Users.FirstOrDefault(x => x.UserName == UserName);
                if (Userlist != null)
                {
                    return Userlist.UserID;
                }
                else
                {
                    return "Error";
                }
            }
            catch (Exception ex)
            {

                return ex.Message;
            }



        }

        public static User FindAudience(string clientId)
        {
            try
            {
                PetServicesEntities dbContext = new PetServicesEntities();
                User user = dbContext.Users.FirstOrDefault(x => x.UserID == clientId);

                return user;

            }
            catch (Exception ex)
            {
                return null;

            }



        }
    }
}