﻿/// <reference path="jquery-3.2.0.min.js" />

var Common = function () {
    _this = this;
    var baseUrl = "http://localhost:18291/";
    _this.baseUrl = baseUrl;
    _this.AjaxCall = function (url, data, methodType, isAsync, callback, btn) {
        var value = $(btn).val();
        if (value == "") {
            value = $(btn).html();
        }
        $(btn).val("Processing...").prop("disabled", true); // for input element
        $(btn).width($(btn).width()).html("<i class='fa fa-circle-o-notch fa-spin fa-fw'></i>").prop("disabled", true); // for button element
        $.ajax({
            type: methodType,
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8', // text for IE, xml for the rest ,
            url: baseUrl + url,
            data: data,
            async: isAsync,
            headers: {
                "Authorization": "Basic " + btoa("admin" + ":" + "admin")
            },
            success: function (response) {
                $(btn).val(value).prop("disabled", false); // for input element
                $(btn).removeAttr("style").html(value).prop("disabled", false); // for button element

                if (response.IsValid == false && response.Message == "LoggedOut") {
                    _this.ShowMessage("You are logged out.", "error");
                    setTimeout(function () {
                        window.location = "/account/login";
                    }, 1000);
                }
                else {
                    if (response.IsValid == false && response.ShowMessage == true) {
                        _this.ShowMessage(response.Message, "error");
                    }
                }

                callback(response);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $(btn).val(value).prop("disabled", false); // for input element
                $(btn).removeAttr("style").html(value).prop("disabled", false); // for button element

                if (jqXhr.getResponseHeader('Content-Type').indexOf('application/json') > -1) {
                    // only parse the response if you know it is JSON
                    var error = $.parseJSON(jqXhr.responseText);
                    _this.ShowMessage(error, "error");
                } else {
                    _this.ShowMessage("Oops! Something went wrong, please try again later.", "error");
                }
                //$(".modal").modal("hide");
            }
        });
    }
    _this.ConfirmAjaxCall = function (url, data, methodType, isAsync, callback, btn, txt) {
        if (txt != "" && txt != null) {
            $("#txtGlobal").text(txt);
        }
        else {
            $("#txtGlobal").text("Are you sure that you want to delete?");
        }

        $("#myGlobalModal").modal("show");

        $("#btnGlobalConfirm").click(function () {
            _this.AjaxCall(url, data, methodType, isAsync, callback, btn);
            $("#myGlobalModal").modal("hide");
        });
    }
    _this.AjaxCallFormData = function (url, data, isAsync, callback, btn) {
        var value = $(btn).val();
        if (value == "") {
            value = $(btn).html();
        }
        $(btn).val("Processing...").prop("disabled", true); // for input element
        $(btn).width($(btn).width()).html("<i class='fa fa-circle-o-notch fa-spin fa-fw'></i>").prop("disabled", true); // for button element

        $.ajax({
            url: baseUrl + url,
            data: data,
            contentType: false,
            processData: false,
            async: isAsync,
            type: 'POST',
            headers: {
                "Authorization": "Basic " + btoa("admin" + ":" + "admin")
            },
            success: function (response) {
                $(btn).val(value).prop("disabled", false); // for input element
                $(btn).removeAttr("style").html(value).prop("disabled", false); // for button element

                if (response.IsValid == false && response.Message == "LoggedOut") {
                    _this.ShowMessage("You are logged out.", "error");
                    setTimeout(function () {
                        window.location = "/account/login";
                    }, 1000);
                }
                else {
                    if (response.IsValid == false && response.ShowMessage == true) {
                        _this.ShowMessage(response.Message, "error");
                    }
                }

                callback(response);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $(btn).val(value).prop("disabled", false); // for input element
                $(btn).removeAttr("style").html(value).prop("disabled", false); // for button element

                if (jqXhr.getResponseHeader('Content-Type').indexOf('application/json') > -1) {
                    // only parse the response if you know it is JSON
                    var error = $.parseJSON(jqXhr.responseText);
                    _this.ShowMessage(error, "error");
                } else {
                    _this.ShowMessage("Oops! Something went wrong, please try again later.", "error");
                }
                //$(".modal").modal("hide");
            }
        });
    }
    _this.Confirm = function (btn, txt) {
        if (txt != "" && txt != null) {
            $("#txtGlobal").text(txt);
        }
        else {
            $("#txtGlobal").text("Are you sure that you want to delete?");
        }
        $("#myGlobalModal").modal("show");
        $("#btnGlobalConfirm").click(function () {
            $(btn).trigger("click");
            $("#myGlobalModal").modal("hide");
            return true;
        });
    }
    _this.ShowMessage = function (msg, type) {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "10000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        toastr[type](msg, type.charAt(0).toUpperCase() + type.slice(1))
    }
    _this.Validate = function (form) {
        // Validation
        if ($(form).length > 0) {
            if (!$(form).validationEngine('validate',
                {
                scroll: true,
                promptPosition: "bottomLeft",
                autoHidePrompt: true
            })) {
                return false;
            }
            else {
                return true;
            }
        }
    }
    _this.ValidateParsley = function (form) {
        // Validation
        if ($(form).length > 0) {
            $(form).parsley().validate();
            if ($('.parsley-error').length === 0) {
                return true;
            }
            else {
                return false;
            }
            //.on('field:validated', function () {
            //    var ok = $('.parsley-error').length === 0;
            //    return ok;
            //});
        }
    }
    _this.GetFormValues = function (form) {
        var json = {};
        $(form).find("input, select, textarea").not("input[type=radio]").each(function () {
            if ($(this).is("[name]")) {
                if ($(this).is(':checkbox')) {
                    json[$(this).attr("name")] = $(this).is(":checked");
                }
                else {
                    json[$(this).attr("name")] = $(this).val();
                }
            }

        });
        return json;
    }
    _this.ClearFormValues = function (form) {
        var json = {};
        $(form).find("input[type=text], select, textarea, input[type=hidden]").each(function () {
            if ($(this).is("[data-element]")) {
                json[$(this).attr("data-element")] = $(this).val("");
            }
        });
        return json;
    }
    _this.ClearAddHTML = function (id,html) {
     
        $('#' + id).html('');
        $('#' + id).html(html);
       
    }
    _this.ClearAddValue = function (id, html) {

        $('#' + id).val('');
        $('#' + id).val(html);

    }
    _this.GetQueryStringParams = function (sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }
    _this.CommaNumber = function (num) {
        return Number(num).toLocaleString('en');
    }
    _this.readImage = function (id) {
        if (this.files && this.files[0]) {
            var FR = new FileReader();
            FR.onload = function (e) {
                $('#' + id).attr("src", e.target.result);
                //$("#base64").val(e.target.result);
            };
            FR.readAsDataURL(this.files[0]);
        }
    }
    _this.readImage = function (file, target, callback) {
        if (file[0].files && file[0].files[0]) {
            var FR = new FileReader();
            FR.onload = function (e) {
                target.attr("src", e.target.result);
                callback(true);
            };
            FR.readAsDataURL(file[0].files[0]);
        }
    }
    _this.isValidPassword = function (input) {
        var reg = /^[^%\s]{6,}$/;
        var reg2 = /[a-zA-Z]/;
        var reg3 = /[0-9]/;
        return reg.test(input) && reg2.test(input) && reg3.test(input);
    }
    _this.copyToClipboard = function (elem) {
        // create hidden text element, if it doesn't already exist
        var targetId = "_hiddenCopyText_";
        var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
        var origSelectionStart, origSelectionEnd;
        if (isInput) {
            // can just use the original source element for the selection and copy
            target = elem;
            origSelectionStart = elem.selectionStart;
            origSelectionEnd = elem.selectionEnd;
        } else {
            // must use a temporary form element for the selection and copy
            target = document.getElementById(targetId);
            if (!target) {
                var target = document.createElement("textarea");
                target.style.position = "absolute";
                target.style.left = "-9999px";
                target.style.top = "0";
                target.id = targetId;
                document.body.appendChild(target);
            }
            target.textContent = elem.textContent;
        }
        // select the content
        var currentFocus = document.activeElement;
        target.focus();
        target.setSelectionRange(0, target.value.length);

        // copy the selection
        var succeed;
        try {
            succeed = document.execCommand("copy");
        } catch (e) {
            succeed = false;
        }
        // restore original focus
        if (currentFocus && typeof currentFocus.focus === "function") {
            currentFocus.focus();
        }

        if (isInput) {
            // restore prior selection
            elem.setSelectionRange(origSelectionStart, origSelectionEnd);
        } else {
            // clear temporary content
            target.textContent = "";
        }
        return succeed;
    }
}

var orderStatus = {
    Pending: "Pending",
    Cooking: "Cooking",
    Cooked: "Prepared",
    Completed: "Completed"
}