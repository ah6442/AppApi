﻿function OpenAddUserModal() {
    $('#ModalAddUser').modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    });
    $('#btnUpdateUser').hide();
    $('#btnAddUser').show();
    $("#txtFirstName").val("");
    $("#txtLastName").val("");
    $("#txtPassword").val("");
    $("#txtUserName").val("");
    $("#txtFirstName").focus();
    $("#txtContact").val("");
    $("#ddlGender").val("male");
    // Permissionlist();
}

$(document).ready(function () {


    var fullDate = new Date();
    var twoDigitDay = (fullDate.getDate() >= 10) ? fullDate.getDate() : '0' + fullDate.getDate();
    var twoDigitMonth = (fullDate.getMonth() + 1 >= 10) ? fullDate.getMonth() + 1 : '0' + (fullDate.getMonth() + 1);
    var currentDate = twoDigitDay + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
    console.log(currentDate);
    $('#txtJoiningDate').val(currentDate);
    $("#dpJoiningDate").datepicker({
        autoclose: true,
    });


    // Permissionlist();
    FillUserData();



});

//$(function () {
//    $('#ddlDesignation').on('change', function () {
//        var RoleID = $(this).val();
//        if (RoleID == '2') {
//            $('#RowPermission').show();
//            Permissionlist();
//        }
//        else {
//            $('#RowPermission').hide();
//            Permissionlist();
//        }
//    });

//});

//function Permissionlist() {
//    $('#ddlPermissions').multiselect({
//        includeSelectAllOption: true,
//        selectAllValue: 'multiselect-all',
//        enableCaseInsensitiveFiltering: true,
//        enableFiltering: true,
//        maxHeight: '300',
//        buttonWidth: '300',
//    });
//    $("#ddlPermissions").multiselect('updateButtonText');


//    $("#ddlPermissions option:selected").removeAttr("selected");
//    $('#ddlPermissions').multiselect('refresh');


//}

function FillUserData() {
    $('#UsersTable').empty();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: "/ajax/GetAdminList",
        success: function (response) {
            var data = response.Data;
            var trHTML = '<thead><tr><th>User Name</th><th>Full Name</th><th>User Contact</th><th>Gender</th><th style="width:10%">Edit</th><th style="width:10%">Delete</th></tr></thead><tbody></tbody>';
            $('#UsersTable').append(trHTML);
            for (j = 0; j < data.length; j++) {
                var trHTML = '';
                trHTML += '<tr><td><span>' + data[j].UserName + '</span></td>';
                trHTML += '<td><span>' + data[j].FirstName + ' ' + data[j].LastName + '</span></td>';
                trHTML += '<td><span>' + data[j].UserContact + '</span></td>';
                trHTML += '<td><span>' + data[j].UserGender + '</span></td>';
                trHTML += "<td><a class='edit btn btn-primary' onclick='UserEditInfo(" + data[j].AdminID + ")';>Edit <i class='fa fa-pencil'></i></a></td>";
                trHTML += "<td><a class='delete btn btn-defaul' onclick='DeleteUser(" + data[j].AdminID + ")'; >Delete <i class='fa fa-times'></i></a></td></tr>";

                $('#UsersTable').append(trHTML);
            }
            $("#GridData").show();

            $('#UsersTable').dataTable();
        }
    });
}

function UserEditInfo(UserID) {

    var Param = { "AdminID": UserID };

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: "/ajax/EditAdmin",
        data: JSON.stringify(Param),
        success: function (response) {

            var data = response.Data;
            //var PermissionsArray = new Array();
            //if (data.d.Permissions == null) {
            //}
            //else {
            //    for (var i = 0; i < data.d.Permissions.length; i++) {
            //        PermissionsArray[i] = data.d.Permissions[i];
            //    }
            //}
            $("#txtFirstName").val(data.FirstName);
            $("#txtLastName").val(data.LastName);
            $("#txtPassword").val(data.Password);
            $("#txtFirstName").focus();
            $("#txtUserName").val(data.UserName);
            $("#txtUserID").val(data.AdminID); $("#txtUserName").val(data.UserName);
            $("#txtContact").val(data.UserContact);
            $("#ddlGender").val(data.UserGender);
            $('#txtJoiningDate').val(data.CreatedDate);
            $('#ModalAddUser').modal({
                show: true,
                backdrop: 'static',
                keyboard: false
            });
            $('#btnUpdateUser').show();
            $('#btnAddUser').hide();

        }
    });
}
function UpdateUser() {

    var user_id = $("#txtUserID").val();
    var user_first_name = $("#txtFirstName").val();
    var user_last_name = $("#txtLastName").val();
    var user_password = $("#txtPassword").val();
    var user_name = $("#txtUserName").val();
    var user_contact = $("#txtContact").val();
    var user_gender = $("#ddlGender option:selected").val();
    var user_joining_date = $('#txtJoiningDate').val();

    if (validate()) {

        var Param = {
            "AdminID": user_id,"Password": user_password,"UserName":user_name, "FirstName": user_first_name, "LastName": user_last_name,
            "UserContact": user_contact, "UserGender": user_gender
        };
        $.ajax({
            method: "POST",
            url: "/ajax/UpdateAdmin",
            data: Param,
            success: function (response) {
                if (response.IsValid) {
                    $('#ModalAddUser').modal('hide');
                    $().toastmessage("showSuccessToast", "User updated sucessfully.");
                    FillUserData();
                }
                else {
                    $().toastmessage("showErrorToast", "Error.");
                }


            }
        });
    }
}
function DeleteUser(UserId) {
    bootbox.confirm({
        message: 'Are you sure you want to delete?',
        buttons: {
            'cancel': {
                label: 'Cancel',
                className: 'btn-default pull-left'
            },
            'confirm': {
                label: 'Delete',
                className: 'green pull-right'
            }
        },

        callback: function (result) {
            if (result) {

                var Param = { "AdminID": UserId };
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: "/ajax/DeleteAdmin",
                    data: JSON.stringify(Param),
                    type: "POST",
                    success: function (response) {

                        if (response.IsValid) {
                            $().toastmessage("showSuccessToast", "User deleted sucessfully.");
                            FillUserData();
                        }
                    }
                });
            }
        }
    });



}

function AddNewUser() {

    var user_first_name = $("#txtFirstName").val();
    var user_last_name = $("#txtLastName").val();
    var user_password = $("#txtPassword").val();
    var user_contact = $("#txtContact").val();
    var user_gender = $("#ddlGender option:selected").val();
    var user_joining_date = $('#txtJoiningDate').val();
   var user_name= $("#txtUserName").val();


    if (validate()) {

        var Param = {
            FirstName: user_first_name,UserName:user_name, Password: user_password,LastName: user_last_name,
            UserContact: user_contact,UserGender: user_gender
        };
        $.ajax({
            method: "POST",
            url: "/ajax/AddAdmin",
            data: Param,
            success: function (response) {
                if (response.IsValid) {
                    $('#ModalAddUser').modal('hide');
                    $().toastmessage("showSuccessToast", "User saved sucessfully.");
                    FillUserData();
                }
                else {
                    $().toastmessage("showErrorToast", "Error .....");
                }

            }
        });
    }
}

function ErrorTextBV(ErrorMsg) {

    $("#ErrorDivBV").show();
    $("#lblErrorBV").text(ErrorMsg);
}

function validateLettersOnly(input) {
    var re = /^([a-zA-Z]+\s)*[a-zA-Z]+$/;
    return re.test(input);
}

function ErrorTextEditUser(ErrorMsg) {

    $("#ErrorDivEdit").show();
    $("#lblErrorEdit").text(ErrorMsg);
}


function validate() {

    var user_joining_date = $('#txtJoiningDate').val();
    var user_name = $("#txtUserName").val();
    var user_first_name = $("#txtFirstName").val();
    var user_last_name = $("#txtLastName").val();
    var user_password = $("#txtPassword").val();
    var user_contact = $("#txtContact").val();
    var user_gender = $("#txtGender").val();

    if (user_joining_date == "") {
        $().toastmessage("showErrorToast", "Please provide User Joining Date.");
        return false;
    }
    if (user_first_name == "") {
        $().toastmessage("showErrorToast", "Please provide First Name.");
        return false;
    }
    if (user_last_name == "") {
        $().toastmessage("showErrorToast", "Please provide Last Name.");
        return false;
    }
    if (user_password == "") {
        $().toastmessage("showErrorToast", "Please provide User Password.");
        return false;
    }
    
    if (user_contact == "") {
        $().toastmessage("showErrorToast", "Please provide Contact Number.");
        return false;
    }

    if (user_gender == "") {
        $().toastmessage("showErrorToast", "Please provide Gender.");
        return false;
    }
    

    return true;
}