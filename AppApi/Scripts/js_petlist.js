﻿var DataPet;
$(document).ready(function () {

    FillPetProfileData();



});

function FillPetProfileData() {
  
   

    $('#PetTable').empty();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: "/Ajax/GetPetList",
        success: function (response) {
            var data = response.Data;
            DataPet = response.Data;
            var trHTML = '<thead><tr><th>Pet Type</th><th>Pet Breed</th><th>Country/City</th><th>Gender</th><th>Veccinated</th><th style="width:10%"></th></tr></thead><tbody></tbody>';
            $('#PetTable').append(trHTML);
            for (j = 0; j < data.length; j++) {
                var trHTML = '';
                trHTML += '<tr><td><span>' + data[j].PetType + '</span></td>';
                trHTML += '<td><span>' + data[j].PetBreed +'</span></td>';
                trHTML += '<td><span>' + data[j].Country + ' ' + data[j].City + '</span></td>';
                trHTML += '<td><span>' + data[j].PetGender + '</span></td>';
                trHTML += '<td><span>' + data[j].IsVeccinated + '</span></td>';
                trHTML += "<td><a class='edit btn btn-primary' onclick='GetDetailPetProfile(" + data[j].PetProfileId + ")';>Detail<i class='fa fa-pencil'></i></a></td></tr>";

                $('#PetTable').append(trHTML);
            }
           
            $('#PetTable').dataTable();
        }
    });
}
function GetDetailPetProfile(id)
{
    $("#txtPetProfileId").val(id);
    objCommon = new Common();
    var dataString = {
        PetProfileId: id
    };

    objCommon.AjaxCall("ajax/GetAllImagesByPetId", $.param(dataString), "GET", true, function (response) {
        //objCommon.ShowMessage(response.Message, "success");
        if (response.IsValid) {
            console.log(DataPet);
            var DataImage=response.Data;
            var html='';
            for(var i=0;i<DataImage.length;i++)
            {
                html+='<div class="col-lg-3 col-sm-3 col-xs-3">';
               html+=' <div class="panel panel-default">';
               html+=' <div class="panel-body">';
               html+=' <a><img src="http://placehold.it/200x200" width="100" class="img-thumbnail img-responsive">';
               html += '</a></div><div class="panel-footer"><a onlick="DeletePetImage(' + DataImage.PetImageId + ')">Delete</a></div></div></div>';
            }
            $("#petimages").append(html);

            $.each(DataPet, function (key, value) {
                if(value.PetProfileId==id)
                {
                    objCommon.ClearAddValue("txtPetType", value.PetType);
                    objCommon.ClearAddValue("txtPetBreed", value.PetBreed);
                    objCommon.ClearAddValue("txtPetGender", value.PetGender);
                    objCommon.ClearAddValue("txtVeccinated", value.IsVeccinated.toString());
                    objCommon.ClearAddValue("txtRescued", value.RescuedOn);
                    objCommon.ClearAddValue("txtCountry", value.Country);
                    objCommon.ClearAddValue("txtCity", value.City);
                }
            });
            $("#ModalPetProfileDetail").modal("show");

        }
    });

    
   

  
}

function RemovePetProfile() {
    bootbox.confirm({
        message: 'Are you sure you want to Remove?',
        buttons: {
            'cancel': {
                label: 'Cancel',
                className: 'btn-default pull-left'
            },
            'confirm': {
                label: 'Delete',
                className: 'green pull-right'
            }
        },

        callback: function (result) {
            if (result) {
                objCommon = new Common();
                var id = $("#txtPetProfileId").val();
                var dataString = {
                    PetProfileId: id
                };

                objCommon.AjaxCall("ajax/RemoveProfileByProfileId", $.param(dataString), "GET", true, function (response) {
                    //objCommon.ShowMessage(response.Message, "success");
                    if (response.IsValid) {

                        objCommon.ShowMessage(response.Message, "success");
                        $("#ModalPetProfileDetail").modal("hide");

                    }
                });


            }
        }
    });


}

function ApprovePetProfile() {
    bootbox.confirm({
        message: 'Are you sure you want to Approve?',
        buttons: {
            'cancel': {
                label: 'Cancel',
                className: 'btn-default pull-left'
            },
            'confirm': {
                label: 'Delete',
                className: 'green pull-right'
            }
        },

        callback: function (result) {
            if (result) {
                objCommon = new Common();
                var id = $("#txtPetProfileId").val();
                var dataString = {
                    PetProfileId: id
                };

                objCommon.AjaxCall("ajax/ApprovedProfileByProfileId", $.param(dataString), "GET", true, function (response) {
                    //objCommon.ShowMessage(response.Message, "success");
                    if (response.IsValid) {
                        objCommon.ShowMessage(response.Message, "success");

                        $("#ModalPetProfileDetail").modal("hide");

                    }
                });


            }
        }
    });


}

function DeletePetImage(imageid) {
    bootbox.confirm({
        message: 'Are you sure you want to delete?',
        buttons: {
            'cancel': {
                label: 'Cancel',
                className: 'btn-default pull-left'
            },
            'confirm': {
                label: 'Delete',
                className: 'green pull-right'
            }
        },

        callback: function (result) {
            if (result) {
                objCommon = new Common();
                var dataString = {
                    PetProfileImageId: imageid
                };

                objCommon.AjaxCall("ajax/RemoveImageByProfileId", $.param(dataString), "GET", true, function (response) {
                    //objCommon.ShowMessage(response.Message, "success");
                    if (response.IsValid) {

                        objCommon.ShowMessage(response.Message, "success");
                        var id = $("#txtPetProfileId").val();
                        GetDetailPetProfile(id);

                    }
                });

            }
        }
    });



}



function ErrorTextBV(ErrorMsg) {

    $("#ErrorDivBV").show();
    $("#lblErrorBV").text(ErrorMsg);
}

function validateLettersOnly(input) {
    var re = /^([a-zA-Z]+\s)*[a-zA-Z]+$/;
    return re.test(input);
}

function ErrorTextEditUser(ErrorMsg) {

    $("#ErrorDivEdit").show();
    $("#lblErrorEdit").text(ErrorMsg);
}
