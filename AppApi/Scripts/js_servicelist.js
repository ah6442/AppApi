﻿var ServiceData;
$(document).ready(function () {

    FillServiceData();



});

function FillServiceData() {
    $('#ServiceTable').empty();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: "/Ajax/GetPetServicesList",
        success: function (response) {
            var data = response.Data;
            ServiceData = data;
            var trHTML = '<thead><tr><th>Profile Image</th><th>Contact Number</th><th>Country</th><th>City</th><th>Status</th><th style="width:10%"></th></tr></thead><tbody></tbody>';
            $('#ServiceTable').append(trHTML);
            for (j = 0; j < data.length; j++) {
                var trHTML = '';
                trHTML += '<tr><td><img src="' + data[j].ProfileImage + '">/></td>';
                trHTML += '<td><span>' + data[j].ContactNumber + '</span></td>';
                trHTML += '<td><span>' + data[j].Country + '</span></td>';
                trHTML += '<td><span>' + data[j].City + '</span></td>';
                trHTML += '<td><span>' + data[j].StatusProfile + '</span></td>';
                trHTML += "<td><a class='edit btn btn-primary' onclick='DetailService(" + data[j].PetCareServicesId + ")';>Detail <i class='fa fa-times'></i></a></td></tr>";

                $('#ServiceTable').append(trHTML);
            }
      

            $('#ServiceTable').dataTable();
        }
    });
}

function DetailService(id) {
    $("#txtPetServiceProfileId").val(id);
    objCommon = new Common();
    var dataString = {
        PetServiceProfileId: id
    };

    objCommon.AjaxCall("ajax/GetAllServiceImagesByPetId", $.param(dataString), "GET", true, function (response) {
        //objCommon.ShowMessage(response.Message, "success");
        if (response.IsValid) {
            console.log(ServiceData);
            var DataImage = response.Data;
            var html = '';
            for (var i = 0; i < DataImage.length; i++) {
                html += '<div class="col-lg-3 col-sm-3 col-xs-3">';
                html += ' <div class="panel panel-default">';
                html += ' <div class="panel-body">';
                html += ' <a><img src="http://placehold.it/200x200" width="100" class="img-thumbnail img-responsive">';
                html += '</a></div><div class="panel-footer"><a onlick="DeletePetServiceImage(' + DataImage.PetImageId + ')">Delete</a></div></div></div>';
            }
            $("#petServiceimages").append(html);

            $.each(ServiceData, function (key, value) {
                if (value.PetCareServicesId == id) {
                    objCommon.ClearAddValue("txtPetCareType", value.PetCareTypeName);
                    objCommon.ClearAddValue("txtContactNumber", value.ContactNumber);
                    objCommon.ClearAddValue("txtFacebookLink", value.FacebookLink);
                    objCommon.ClearAddValue("txtTwitterLink", value.TwitterLink);
                    objCommon.ClearAddValue("txtWebsiteLink", value.WebsiteLink);
                    objCommon.ClearAddValue("txtCountry", value.Country);
                    objCommon.ClearAddValue("txtCity", value.City);
                    objCommon.ClearAddValue("txtAddress", value.Address);
                    objCommon.ClearAddValue("txtDescription", value.Description);
                }
            });
            $("#ModalPetServiceProfileDetail").modal("show");

        }
    });





}

function RemovePetServiceProfile() {
    bootbox.confirm({
        message: 'Are you sure you want to Remove?',
        buttons: {
            'cancel': {
                label: 'Cancel',
                className: 'btn-default pull-left'
            },
            'confirm': {
                label: 'Delete',
                className: 'green pull-right'
            }
        },

        callback: function (result) {
            if (result) {
                objCommon = new Common();
                var id = $("#txtPetServiceProfileId").val();
                var dataString = {
                    PetServiceProfileId: id
                };

                objCommon.AjaxCall("ajax/RemoveServiceProfileByProfileId", $.param(dataString), "GET", true, function (response) {
                    //objCommon.ShowMessage(response.Message, "success");
                    if (response.IsValid) {

                        objCommon.ShowMessage(response.Message, "success");
                        $("#ModalPetServiceProfileDetail").modal("hide");

                    }
                });


            }
        }
    });


}

function ApprovePetServiceProfile() {
    bootbox.confirm({
        message: 'Are you sure you want to Approve?',
        buttons: {
            'cancel': {
                label: 'Cancel',
                className: 'btn-default pull-left'
            },
            'confirm': {
                label: 'Delete',
                className: 'green pull-right'
            }
        },

        callback: function (result) {
            if (result) {
                objCommon = new Common();
                var id = $("#txtPetServiceProfileId").val();
                var dataString = {
                    PetServiceProfileId: id
                };

                objCommon.AjaxCall("ajax/ApprovedServiceProfileByProfileId", $.param(dataString), "GET", true, function (response) {
                    //objCommon.ShowMessage(response.Message, "success");
                    if (response.IsValid) {
                        objCommon.ShowMessage(response.Message, "success");

                        $("#ModalPetServiceProfileDetail").modal("hide");

                    }
                });


            }
        }
    });


}

function DeletePetServiceImage(imageid) {
    bootbox.confirm({
        message: 'Are you sure you want to delete?',
        buttons: {
            'cancel': {
                label: 'Cancel',
                className: 'btn-default pull-left'
            },
            'confirm': {
                label: 'Delete',
                className: 'green pull-right'
            }
        },

        callback: function (result) {
            if (result) {
                objCommon = new Common();
                var dataString = {
                    PetServiceProfileImageId: imageid
                };

                objCommon.AjaxCall("ajax/RemoveServiceImageByProfileId", $.param(dataString), "GET", true, function (response) {
                    //objCommon.ShowMessage(response.Message, "success");
                    if (response.IsValid) {

                        objCommon.ShowMessage(response.Message, "success");
                        var id = $("#txtPetServiceProfileId").val();
                        DetailService(id);

                    }
                });

            }
        }
    });



}

function ErrorTextBV(ErrorMsg) {

    $("#ErrorDivBV").show();
    $("#lblErrorBV").text(ErrorMsg);
}

function validateLettersOnly(input) {
    var re = /^([a-zA-Z]+\s)*[a-zA-Z]+$/;
    return re.test(input);
}

function ErrorTextEditUser(ErrorMsg) {

    $("#ErrorDivEdit").show();
    $("#lblErrorEdit").text(ErrorMsg);
}


function validate() {

    var user_joining_date = $('#txtJoiningDate').val();
    var user_name = $("#txtUserName").val();
    var user_first_name = $("#txtFirstName").val();
    var user_last_name = $("#txtLastName").val();
    var user_password = $("#txtPassword").val();
    var user_contact = $("#txtContact").val();
    var user_gender = $("#txtGender").val();

    if (user_joining_date == "") {
        $().toastmessage("showErrorToast", "Please provide User Joining Date.");
        return false;
    }
    if (user_first_name == "") {
        $().toastmessage("showErrorToast", "Please provide First Name.");
        return false;
    }
    if (user_last_name == "") {
        $().toastmessage("showErrorToast", "Please provide Last Name.");
        return false;
    }
    if (user_password == "") {
        $().toastmessage("showErrorToast", "Please provide User Password.");
        return false;
    }
    
    if (user_contact == "") {
        $().toastmessage("showErrorToast", "Please provide Contact Number.");
        return false;
    }

    if (user_gender == "") {
        $().toastmessage("showErrorToast", "Please provide Gender.");
        return false;
    }
    

    return true;
}