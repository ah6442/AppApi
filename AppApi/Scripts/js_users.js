﻿/// <reference path="jquery.common.js" />

$(document).ready(function () {

    FillUserData();



});

function FillUserData() {
    $('#UsersTable').empty();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: "/Ajax/GetUserList",
        success: function (response) {
            var data = response.Data;
            var trHTML = '<thead><tr><th>User Name</th><th>Full Name</th><th>Country</th><th>City</th><th>Provider</th><th style="width:20%"></th></tr></thead><tbody></tbody>';
            $('#UsersTable').append(trHTML);
            for (j = 0; j < data.length; j++) {
                var trHTML = '';
                trHTML += '<tr><td><span>' + data[j].UserName + '</span></td>';
                trHTML += '<td><span>' + data[j].FullName +'</span></td>';
                trHTML += '<td><span>' + data[j].Country + '</span></td>';
                trHTML += '<td><span>' + data[j].City + '</span></td>';
                trHTML += '<td><span>' + data[j].ProviderName + '</span></td>';
                trHTML += "<td><a class='edit btn btn-primary' onclick='ApproveUser(" + data[j].UserID + ")';>Approve <i class='fa fa-pencil'></i></a><a class='delete btn btn-defaul' onclick='BlockUnblockUser(" + data[j].UserID + ")'; > Block <i class='fa fa-times'></i></a></td></tr>";

                $('#UsersTable').append(trHTML);
            }
            $("#GridData").show();

            $('#UsersTable').dataTable();
        }
    });
}

function BlockUnblockUser(UserId) {
    bootbox.confirm({
        message: 'Are you sure you want to delete?',
        buttons: {
            'cancel': {
                label: 'Cancel',
                className: 'btn-default pull-left'
            },
            'confirm': {
                label: 'Delete',
                className: 'green pull-right'
            }
        },

        callback: function (result) {
            if (result) {
                objCommon = new Common();
                var dataString = {
                    UserId: UserId
                };

                objCommon.AjaxCall("ajax/UserBlockUnblock", $.param(dataString), "GET", true, function (response) {
                    //objCommon.ShowMessage(response.Message, "success");
                    if (response.IsValid) {

                        objCommon.ShowMessage(response.Message, "success");
                      

                    }
                });
            }
        }
    });



}

function ApproveUser(UserId) {
    bootbox.confirm({
        message: 'Are you sure you want to Approved User?',
        buttons: {
            'cancel': {
                label: 'Cancel',
                className: 'btn-default pull-left'
            },
            'confirm': {
                label: 'Confirm',
                className: 'green pull-right'
            }
        },

        callback: function (result) {
            if (result) {

                var Param = { "AdminID": UserId };
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: "/Ajax/ApprovedUser",
                    data: JSON.stringify(Param),
                    type: "POST",
                    success: function (response) {

                        if (response.IsValid) {
                            $().toastmessage("showSuccessToast", "User Approved sucessfully.");
                            FillUserData();
                        }
                    }
                });
            }
        }
    });



}


function ErrorTextBV(ErrorMsg) {

    $("#ErrorDivBV").show();
    $("#lblErrorBV").text(ErrorMsg);
}

function validateLettersOnly(input) {
    var re = /^([a-zA-Z]+\s)*[a-zA-Z]+$/;
    return re.test(input);
}

function ErrorTextEditUser(ErrorMsg) {

    $("#ErrorDivEdit").show();
    $("#lblErrorEdit").text(ErrorMsg);
}


function validate() {

    var user_joining_date = $('#txtJoiningDate').val();
    var user_name = $("#txtUserName").val();
    var user_first_name = $("#txtFirstName").val();
    var user_last_name = $("#txtLastName").val();
    var user_password = $("#txtPassword").val();
    var user_contact = $("#txtContact").val();
    var user_gender = $("#txtGender").val();

    if (user_joining_date == "") {
        $().toastmessage("showErrorToast", "Please provide User Joining Date.");
        return false;
    }
    if (user_first_name == "") {
        $().toastmessage("showErrorToast", "Please provide First Name.");
        return false;
    }
    if (user_last_name == "") {
        $().toastmessage("showErrorToast", "Please provide Last Name.");
        return false;
    }
    if (user_password == "") {
        $().toastmessage("showErrorToast", "Please provide User Password.");
        return false;
    }
    
    if (user_contact == "") {
        $().toastmessage("showErrorToast", "Please provide Contact Number.");
        return false;
    }

    if (user_gender == "") {
        $().toastmessage("showErrorToast", "Please provide Gender.");
        return false;
    }
    

    return true;
}